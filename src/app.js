const express = require("express");
const serverless = require("serverless-http");

const app = express();
const router = express.Router();

app.use("/.netlify/functions/app", router);

router.get("/", (req, res) => {
  res.json({
    "message": "Hi",
  });
});

router.get("/test", (req, res) => {
  res.json({
    "message": "Test",
  });
});

module.exports.handler = serverless(app);
